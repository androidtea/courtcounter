package com.example.android.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int totalScoreTeamA = 0;
    int totalScoreTeamB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            totalScoreTeamA = savedInstanceState.getInt("totalScoreTeamA");
            totalScoreTeamB = savedInstanceState.getInt("totalScoreTeamB");
        }
        setContentView(R.layout.activity_main);
        displayForTeamA(totalScoreTeamA);
        displayForTeamB(totalScoreTeamB);
    }

    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }

    public void displayForTeamB(int score) {
        TextView scoreView = (TextView) findViewById(R.id.team_b_score);
        scoreView.setText(String.valueOf(score));
    }

    public void addOneForTeamA(View view) {
        int addScore = 1;
        totalScoreTeamA += addScore;
        displayForTeamA(totalScoreTeamA);
    }

    public void addOneForTeamB(View view) {
        int addScore = 1;
        totalScoreTeamB += addScore;
        displayForTeamB(totalScoreTeamB);
    }

    public void addTwoForTeamA(View view) {
        int addScore = 2;
        totalScoreTeamA += addScore;
        displayForTeamA(totalScoreTeamA);
    }

    public void addTwoForTeamB(View view) {
        int addScore = 2;
        totalScoreTeamB += addScore;
        displayForTeamB(totalScoreTeamB);
    }

    public void addThreeForTeamA(View view) {
        int addScore = 3;
        totalScoreTeamA += addScore;
        displayForTeamA(totalScoreTeamA);
    }

    public void addThreeForTeamB(View view) {
        int addScore = 3;
        totalScoreTeamB += addScore;
        displayForTeamB(totalScoreTeamB);
    }

    public void resetScore(View view) {
        totalScoreTeamA = 0;
        totalScoreTeamB = 0;
        displayForTeamA(totalScoreTeamA);
        displayForTeamB(totalScoreTeamB);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("totalScoreTeamA", totalScoreTeamA);
        outState.putInt("totalScoreTeamB", totalScoreTeamB);
        super.onSaveInstanceState(outState);
    }

}
